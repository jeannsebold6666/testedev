import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () =>
      import('../views/Home.vue')
  },
  {
    path: '/character/:id',
    name: 'Character',

    component: () =>
      import('../views/Character.vue')
  },
  {
    path: '/episodies/',
    name: 'Episodies',
    component: () =>
      import('../views/Episodies.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
